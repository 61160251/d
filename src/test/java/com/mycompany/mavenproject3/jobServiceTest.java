/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject3;

import java.time.LocalDate;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author User
 */
public class jobServiceTest {
    
    public jobServiceTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of checkEnableTime method, of class jobService.
     */
    @Test
    public void testCheckEnableTimeTodayIsBetweenStartTimeAndEndTime() {
        System.out.println("checkEnableTime");
        LocalDate startTime = LocalDate.of(2021,10,2);
        LocalDate endTime = LocalDate.of(2021,10,10);
        LocalDate today =  LocalDate.of(2021,10,3);
        boolean expResult = true;
        boolean result = jobService.checkEnableTime(startTime, endTime, today);
        assertEquals(expResult, result);

    }
    @Test
    public void testCheckEnableTimeTodayIsBeforeStartTime() {
        System.out.println("checkEnableTime");
        LocalDate startTime = LocalDate.of(2021,10,2);
        LocalDate endTime = LocalDate.of(2021,10,10);
        LocalDate today =  LocalDate.of(2021,10,1);
        boolean expResult = false;
        boolean result = jobService.checkEnableTime(startTime, endTime, today);
        assertEquals(expResult, result);

    }
    public void testCheckEnableTimeTodayIsAfterEndTime() {
        System.out.println("checkEnableTime");
        LocalDate startTime = LocalDate.of(2021,10,2);
        LocalDate endTime = LocalDate.of(2021,10,10);
        LocalDate today =  LocalDate.of(2021,10,11);
        boolean expResult = false;
        boolean result = jobService.checkEnableTime(startTime, endTime, today);
        assertEquals(expResult, result);

    }
    public void testCheckEnableTimeTodayIsEqualEndTime() {
        System.out.println("checkEnableTime");
        LocalDate startTime = LocalDate.of(2021,10,2);
        LocalDate endTime = LocalDate.of(2021,10,10);
        LocalDate today =  LocalDate.of(2021,10,10);
        boolean expResult = true;
        boolean result = jobService.checkEnableTime(startTime, endTime, today);
        assertEquals(expResult, result);

    }
    public void testCheckEnableTimeTodayIsEqualStratTime() {
        System.out.println("checkEnableTime");
        LocalDate startTime = LocalDate.of(2021,10,2);
        LocalDate endTime = LocalDate.of(2021,10,10);
        LocalDate today =  LocalDate.of(2021,10,2);
        boolean expResult = true;
        boolean result = jobService.checkEnableTime(startTime, endTime, today);
        assertEquals(expResult, result);

    }
    
}
